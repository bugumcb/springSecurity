package com.imooc.security.browser.support;

/**
 * .
 * Created by mcbbugu
 * 2019-08-31 23:58
 */
public class SimpleResponse {

    public SimpleResponse(Object content) {
        this.content = content;
    }

    private Object content;

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }
}