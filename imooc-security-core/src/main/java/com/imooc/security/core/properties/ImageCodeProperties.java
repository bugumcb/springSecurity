package com.imooc.security.core.properties;

import lombok.Data;

/**
 * .
 * Created by mcbbugu
 * 2019-09-02 02:03
 */
@Data
public class ImageCodeProperties {

    private int width = 23;

    private int height = 67;

    private int length = 4;

    //到期时间
    private int expireIn = 60;

    private String url = "/user/*";
}