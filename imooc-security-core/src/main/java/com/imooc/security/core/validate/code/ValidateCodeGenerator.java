package com.imooc.security.core.validate.code;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * .
 * Created by mcbbugu
 * 2019-09-04 22:08
 */
public interface ValidateCodeGenerator {
    ImageCode generate(ServletWebRequest request);
}
