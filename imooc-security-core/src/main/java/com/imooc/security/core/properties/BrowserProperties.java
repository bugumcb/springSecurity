package com.imooc.security.core.properties;

import lombok.Data;

/**
 * .
 * Created by mcbbugu
 * 2019-09-01 00:03
 */
@Data
public class BrowserProperties {

    private String loginPage = "/imooc-signIn.html";

    private LoginType loginType = LoginType.JSON;

    private int remeberMeSeconds = 3600;
}