package com.imooc.security.core.properties;

import lombok.Data;

/**
 * .
 * Created by mcbbugu
 * 2019-09-02 02:05
 */
@Data
public class ValidateCodeProperties {

    private ImageCodeProperties image = new ImageCodeProperties();
}