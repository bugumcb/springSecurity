package com.imooc.security.core.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 权限控制核心配置类
 * Created by mcbbugu
 * 2019-09-01 00:03
 */
@Data
@ConfigurationProperties(prefix = "imooc.security")
public class SecurityProperties {

    //浏览器配置
    private BrowserProperties browser = new BrowserProperties();

    //验证码配置
    private ValidateCodeProperties code = new ValidateCodeProperties();
}