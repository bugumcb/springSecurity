package com.imooc.wiremock;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

/**
 * 模拟服务
 * Created by mcbbugu
 * 2019-08-31 16:46
 */
public class MockServer {

    public static void main(String[] args) throws IOException {
        configureFor(8081);
        removeAllMappings();

        mock("/order/1", "01");
    }

    private static void mock(String url, String file) throws IOException {
        ClassPathResource resource = new ClassPathResource("mock/response/" + file + ".txt");
        String content = (String) StringUtils.join(
                FileUtils.readLines(resource.getFile(), "UTF-8"), "\n");
        stubFor(get(urlPathEqualTo(url)).
                willReturn(aResponse().withBody("{\"{id\":1}").withStatus(200)));
    }
}