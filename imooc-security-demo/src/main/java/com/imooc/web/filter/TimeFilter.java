package com.imooc.web.filter;

import javax.servlet.*;
import java.io.IOException;
import java.util.Date;

/**
 * .
 * Created by mcbbugu
 * 2019-08-31 10:45
 */
//@Component
public class TimeFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("time filter init");
    }

    @Override
    public void doFilter(ServletRequest Request, ServletResponse Response,
                         FilterChain Chain) throws IOException, ServletException {
        System.out.println("time filter start");
        long start = new Date().getTime();
        Chain.doFilter(Request, Response);
        System.out.println("time filter 耗时= " + (new Date().getTime() - start));
        System.out.println("time filter finish");
    }

    @Override
    public void destroy() {
        System.out.println("time filter destory");
    }
}