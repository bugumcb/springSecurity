package com.imooc.web.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * .拦截器
 * Created by mcbbugu
 * 2019-08-31 11:05
 */
@Component
public class Timeinterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest Request, HttpServletResponse Response, Object handler) throws Exception {
        System.out.println("preHandle");
        System.out.println(((HandlerMethod)handler).getBean().getClass().getName());
        System.out.println(((HandlerMethod)handler).getMethod().getName());
        Request.setAttribute("startTime", new Date().getTime());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest Request, HttpServletResponse Response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("postHandle");
        Long start = (Long) Request.getAttribute("startTime");
        System.out.println(("time interceptor 耗时：" + (new Date().getTime() - start)));
    }

    @Override
    public void afterCompletion(HttpServletRequest Request, HttpServletResponse Response, Object handler, Exception e) throws Exception {
        System.out.println("atferCompletion");
        Long start = (Long) Request.getAttribute("startTime");
        System.out.println(("time interceptor 耗时：" + (new Date().getTime() - start)));
        System.out.println("e is " + e);
    }
}