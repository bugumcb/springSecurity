package com.imooc.web.controller;

import com.imooc.dao.FileInfo;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;

/**
 * .
 * Created by mcbbugu
 * 2019-08-31 13:09
 */
@RestController
@RequestMapping("/file")
public class FileController {

    String folder = "C:\\Users\\mcb45\\Documents\\IdeaProjects\\p1o6vt1\\imooc-security-demo\\src\\main\\java\\com";

    @PostMapping
    public FileInfo upload(MultipartFile file) throws IOException {
        System.out.println(file.getName());
        System.out.println(file.getOriginalFilename());
        System.out.println(file.getSize());

        File localFile = new File(folder, new Date().getTime() + ".txt");
        file.transferTo(localFile);
        return new FileInfo(localFile.getAbsolutePath());
    }

    @GetMapping("/{id}")
    public void download(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) throws IOException {
        //try(){}自动关闭流
        try (InputStream inputStream = new FileInputStream(new File(folder, id + ".txt"));
                OutputStream outputStream = response.getOutputStream();){
            response.setContentType("application/x-download");
            response.addHeader("Content-Disposition", "attachment;filename=test.txt");
            // 输入流拷贝到输出流，结果就是下载
            IOUtils.copy(inputStream, outputStream);
            outputStream.flush();
        }
    }
}