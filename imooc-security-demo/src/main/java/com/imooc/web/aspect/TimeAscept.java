package com.imooc.web.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import java.util.Date;

/**
 * 切片类
 * Created by mcbbugu
 * 2019-08-31 11:55
 */
@Aspect
//@Component
public class TimeAscept {
    @Around("execution(* com.imooc.web.controller.UserController.*(..))")
    public Object handleControllerMethod(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("time aspect start");
        // 方法参数数组
        Object[] args = pjp.getArgs();
        for(Object arg: args){
            System.out.println("arg is " + arg);
        }
        long start = new Date().getTime();
        Object object = pjp.proceed();
        System.out.println("time ascept 耗时= " + (new Date().getTime() - start));
        return object;
    }
}