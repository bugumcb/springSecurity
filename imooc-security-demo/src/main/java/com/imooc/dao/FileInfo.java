package com.imooc.dao;

/**
 * .
 * Created by mcbbugu
 * 2019-08-31 13:10
 */
public class FileInfo {

    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public FileInfo(String path) {
        this.path = path;
    }
}