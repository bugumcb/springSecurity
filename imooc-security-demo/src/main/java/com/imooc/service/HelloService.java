package com.imooc.service;

/**
 * .
 * Created by mcbbugu
 * 2019-08-30 01:15
 */
public interface HelloService {

    String greeting(String name);
}