package com.imooc.service.impl;

import com.imooc.service.HelloService;
import org.springframework.stereotype.Service;

/**
 * .
 * Created by mcbbugu
 * 2019-08-30 01:16
 */
@Service
public class HelloServiceImpl implements HelloService {

    @Override
    public String greeting(String name) {
        System.out.println("greenting");
        return "hello" + name;
    }
}