package com.imooc.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注解
 * Created by mcbbugu
 * 2019-08-30 01:06
 */
// 能标注在什么位置
@Target({ElementType.METHOD, ElementType.FIELD})
//运行时注解？
@Retention(RetentionPolicy.RUNTIME)
//该注解的校验逻辑由谁来执行
@Constraint(validatedBy = MyConstraintValidator.class)
public @interface MyConstraint{

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}